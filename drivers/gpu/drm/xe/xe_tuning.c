// SPDX-License-Identifier: MIT
/*
 * Copyright © 2022 Intel Corporation
 */

#include "xe_wa.h"

#include "xe_platform_types.h"
#include "xe_gt_types.h"
#include "xe_rtp.h"

#include "../i915/gt/intel_gt_regs.h"

static const struct xe_rtp_entry gt_tunings[] = {
	{ XE_RTP_NAME("Tuning: 32B Access Enable"),
	  XE_RTP_RULES(PLATFORM(DG2)),
	  XE_RTP_SET(XEHP_SQCM, EN_32B_ACCESS)
	},
	{}
};

static const struct xe_rtp_entry context_tunings[] = {
	{ XE_RTP_NAME("1604555607"),
	  XE_RTP_RULES(GRAPHICS_VERSION(1200)),
	  XE_RTP_FIELD_SET_NO_READ_MASK(XEHP_FF_MODE2, FF_MODE2_TDS_TIMER_MASK,
					FF_MODE2_TDS_TIMER_128)
	},
	{}
};

void xe_tuning_process_gt(struct xe_gt *gt)
{
	xe_rtp_process(gt_tunings, &gt->reg_sr, gt, NULL);
}
