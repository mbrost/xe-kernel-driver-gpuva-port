/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2022 Intel Corporation
 */

#ifndef _XE_REG_SR_
#define _XE_REG_SR_

#include "i915_reg_defs.h"
#include "xe_reg_sr_types.h"

/*
 * Reg save/restore bookkeeping
 */

struct xe_device;
struct xe_gt;

int xe_reg_sr_init(struct xe_reg_sr *sr, const char *name, struct xe_device *xe);
int xe_reg_sr_dump_kv(struct xe_reg_sr *sr,
		      struct xe_reg_sr_kv **dst);

int xe_reg_sr_add(struct xe_reg_sr *sr, i915_reg_t reg,
		  const struct xe_reg_sr_entry *e);
void xe_reg_sr_apply_mmio(struct xe_reg_sr *sr, struct xe_gt *gt);

#endif
