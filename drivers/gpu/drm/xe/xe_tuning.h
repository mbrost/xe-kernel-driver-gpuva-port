/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2022 Intel Corporation
 */

#ifndef _XE_TUNING_
#define _XE_TUNING_

struct xe_gt;

void xe_tuning_process_gt(struct xe_gt *gt);

#endif
